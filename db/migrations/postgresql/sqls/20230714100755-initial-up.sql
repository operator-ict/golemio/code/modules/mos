-- mos_be_accounts definition

CREATE TABLE mos_be_accounts (
	count int4 NULL,
	"day" varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_accounts_pkey PRIMARY KEY (day)
);


-- mos_be_coupons definition

CREATE TABLE mos_be_coupons (
	coupon_custom_status_id int4 NULL,
	coupon_id int4 NOT NULL,
	created varchar(255) NULL,
	customer_id int4 NULL,
	customer_profile_name varchar(255) NULL,
	description text NULL,
	price numeric NULL,
	seller_id int4 NULL,
	tariff_id int4 NULL,
	tariff_int_name varchar(255) NULL,
	tariff_name varchar(255) NULL,
	tariff_profile_name varchar(255) NULL,
	valid_from varchar(255) NULL,
	valid_till varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	created_by_id varchar(50) NULL, -- CreatedByID - Uživatel který kupon vytvořil¶- 1 - OICT¶- 53 - DPP (eshop i přepážky - v budoucnu snad rozdělení právě pomocí nového uživatele na dva kanály)¶- 188 - MobApp¶
	order_status int4 NULL, -- Stav objednávky¶- 1 - Ordered¶- 2 - PaidByCard¶- 3 - PaidByBankTransfer¶- 4 - Cancelled¶- 5 - PaidByCash
	order_payment_type int4 NULL, -- Způsob platby objednávky¶- 1 - ByCard¶- 2 - PaidByBankTransfer¶- 3 - PaidByCardInOffice¶- 4 - PaidByCashInOffice
	token_id varchar(50) NULL, -- id tokenu - identifikátoru)
	CONSTRAINT mos_be_coupons_pkey PRIMARY KEY (coupon_id)
);

-- Column comments

COMMENT ON COLUMN mos_be_coupons.created_by_id IS 'CreatedByID - Uživatel který kupon vytvořil
-	1 - OICT
-	53 - DPP (eshop i přepážky - v budoucnu snad rozdělení právě pomocí nového uživatele na dva kanály)
-	188 - MobApp
';
COMMENT ON COLUMN mos_be_coupons.order_status IS 'Stav objednávky
-	1 - Ordered
-	2 - PaidByCard
-	3 - PaidByBankTransfer
-	4 - Cancelled
-	5 - PaidByCash';
COMMENT ON COLUMN mos_be_coupons.order_payment_type IS 'Způsob platby objednávky
-	1 - ByCard
-	2 - PaidByBankTransfer
-	3 - PaidByCardInOffice
-	4 - PaidByCashInOffice';
COMMENT ON COLUMN mos_be_coupons.token_id IS 'id tokenu - identifikátoru)';


-- mos_be_customers definition

CREATE TABLE mos_be_customers (
	customer_id int4 NOT NULL,
	date_of_birth varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_customers_pkey PRIMARY KEY (customer_id)
);


-- mos_be_tokens definition

CREATE TABLE mos_be_tokens (
	token_id varchar(50) NOT NULL,
	identifier_type varchar(50) NULL,
	created timestamptz NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_tokens_pkey PRIMARY KEY (token_id)
);


-- mos_be_zones definition

CREATE TABLE mos_be_zones (
	coupon_id int4 NOT NULL,
	zone_name varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_zones_pkey PRIMARY KEY (coupon_id, zone_name)
);


-- mos_ma_devicemodels definition

CREATE TABLE mos_ma_devicemodels (
	id bigserial NOT NULL,
	count int4 NULL,
	model varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_devicemodels_pkey PRIMARY KEY (id)
);


-- mos_ma_ticketactivations definition

CREATE TABLE mos_ma_ticketactivations (
	"date" timestamptz NULL,
	lat numeric NULL,
	lon numeric NULL,
	ticket_id int4 NOT NULL,
	"type" varchar(255) NULL,
	zones varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketactivations_pkey PRIMARY KEY (ticket_id)
);


-- mos_ma_ticketinspections definition


CREATE TABLE mos_ma_ticketinspections (
	"date" timestamptz NULL,
	id bigserial NOT NULL,
	lat numeric NULL,
	lon numeric NULL,
	reason varchar(255) NULL,
	"result" bool NULL,
	user_id varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketinspections_pkey PRIMARY KEY (id)
);


-- mos_ma_ticketpurchases definition

CREATE TABLE mos_ma_ticketpurchases (
	account_id varchar(255) NULL,
	cptp int4 NULL,
	"date" timestamptz NULL,
	duration int4 NULL,
	lat numeric NULL,
	lon numeric NULL,
	tariff_id int4 NULL,
	tariff_name varchar(255) NULL,
	ticket_id int4 NOT NULL,
	zone_count int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketpurchases_pkey PRIMARY KEY (ticket_id)
);