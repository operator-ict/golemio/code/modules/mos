CREATE TABLE mos_be_visited_places_pvp(
    place_id int4 NOT NULL, 
    place_name varchar(255) NOT NULL,
    geom geometry NOT NULL,
    created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
    CONSTRAINT mos_be_visited_places_pkey PRIMARY KEY (place_id)
);

CREATE TABLE mos_be_vouchers_pvp(
    bundle_id int4 NOT NULL, 
    identifier_type varchar(255) NOT NULL,
    tariff_profile varchar(255) NOT NULL,
    customer_profile varchar(255) NOT NULL,
    valid_from timestamptz NOT NULL,
    valid_to timestamptz NOT NULL,
    created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
    CONSTRAINT mos_be_vouchers_pvp_pkey PRIMARY KEY (bundle_id)
);

CREATE TABLE mos_be_visits_pvp(
    place_id int4 NOT NULL,
    bundle_id int4 NOT NULL,
    visited timestamptz NOT NULL,
    created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
   CONSTRAINT mos_be_visits_pvp_pkey PRIMARY KEY (place_id,bundle_id,visited)
);

