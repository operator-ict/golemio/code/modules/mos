# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.16] - 2024-12-03

### Fixed

-   AsyncAPI documentation fix ([integration-engine#264](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/264))

## [1.1.15] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#264](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/264))

## [1.1.14] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2] - 2024-07-04

### Changed

-   Utilize CsvParserMiddleware to parse csv push data ([ig#82](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/82))

## [1.1.12] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.11] - 2023-12-06

### Changed

-   Integration for prague visitor pass ([P0260#1](https://gitlab.com/operator-ict/golemio/projekty/oict/prague-visitor-pass/-/issues/1))

## [1.1.10] - 2023-08-30

-   No changelog

## [1.1.9] - 2023-08-23

### Changed

-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.1.8] - 2023-08-07

### Changed

-   Creating api docs and adding portman [mos#6](https://gitlab.com/operator-ict/golemio/code/modules/mos/-/issues/6)

## [1.1.7] - 2023-07-24

### Changed

-   schema separation [mos#4](https://gitlab.com/operator-ict/golemio/code/modules/mos/-/issues/4)
-   materialized view v_ropidbi_ticket moved to dbt

## [1.1.6] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.4] - 2023-05-29

### Changed

-   update body-parser-csv lib

## [1.1.3] - 2023-05-03

### Changed

-   Adjust IG logger emitter imports

## [1.1.2] - 2023-03-13

### Changed

-   Mongoose validation replaced for json schema validations.

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.8] - 2022-11-18

### Changed

-   Update gitlab-ci template

## [1.0.7] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.6] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

### Fixed

-   Fixed strict validation for TicketActivations, TicketInspections, TicketPurchases
