
# Zpětné donahrání dat po výpadku zdroje

Pokud se stane, že vypadne datový zdroj (neposílá data nebo jsou data nevalidní), a je nutné donahrát data přímo z vygenerovaný csv, lze použít tento postup.

Nejčastěji se stane to, že jsou chybějící data poslaná zvlášť v zipu, který obsahuje jednotlivá csv. Pozor na kódování.

## SQL dotaz pro dohledání chybějících záznamů

Vrací seznam dnů, pro které data chybí. Pokud nevrátí nic, je všechno v pořádku.

```sql
select to_char(den,'dd.mm.yyyy')
from
(select * from generate_series('2020-01-01', current_date-interval '1 days', interval'1 day') den) KALENDAR
left join (
    select created::DATE,COUNT(*)
    from mos.mos_be_coupons
    group by 1
) A
on KALENDAR.DEN = a.created::DATE
where a.created is null
;
```

## Postup donahrání dat

### Kontrola typu souboru

Pokud soubory jsou v windows-1250, je nutné je konvertovat do utf-8.

```bash
iconv -f WINDOWS-1250 -t UTF-8 2022-05-16/accounts.csv -o 2022-05-16/accounts_utf8.csv
```

### Odeslání na input-gateway

```bash
curl --location --request POST 'https://api.golemio.cz/input-gateway/v1/mos-be/accounts' --data-binary '@/path/to/exports/2022-05-16/accounts_utf8.csv' --header 'Content-Type: text/csv' --header 'x-access-token: <internal_input_mosbe@golemio.cz apikey>'
```

### Kompletní bash skript

Adresářová struktura exportu musí být následující:

```bash
├── 2022-05-16
│   ├── accounts.csv
│   ├── cupons.csv
│   ├── customers.csv
│   ├── tokens.csv
│   └── zones.csv
├── 2022-05-17
│   ├── accounts.csv
│   ├── cupons.csv
│   ├── customers.csv
│   ├── tokens.csv
│   └── zones.csv
├── atd.
├── upload.sh
```

Do složky, která obsahuje složky s csv soubory, vložte následující skript pojmenovaný `upload.sh` a spusťte.

```bash
#!/bin/bash

# $base_url = "https://rabin.golemio.cz/input-gateway/v1/mos-be"
base_url="https://api.golemio.cz/input-gateway/v1/mos-be"
token="<internal_input_mosbe@golemio.cz apikey>"

echo "${base_url}"
echo "${token}"

for filename in $(ls -d */)
do
    echo "Started: ${filename}"
    echo "accounts"
    iconv -f WINDOWS-1250 -t UTF-8 ${filename}accounts.csv -o ${filename}accounts_utf8.csv
    curl --location --request POST "${base_url}/accounts" --data-binary "@${filename}accounts_utf8.csv" --header 'Content-Type: text/csv' --header "x-access-token: ${token}"

    echo "coupons"
    iconv -f WINDOWS-1250 -t UTF-8 ${filename}cupons.csv -o ${filename}cupons_utf8.csv
    curl --location --request POST "${base_url}/coupons" --data-binary "@${filename}cupons_utf8.csv" --header 'Content-Type: text/csv' --header "x-access-token: ${token}"

    echo "customers"
    iconv -f WINDOWS-1250 -t UTF-8 ${filename}customers.csv -o ${filename}customers_utf8.csv
    curl --location --request POST "${base_url}/customers" --data-binary "@${filename}customers_utf8.csv" --header 'Content-Type: text/csv' --header "x-access-token: ${token}"

    echo "tokens"
    iconv -f WINDOWS-1250 -t UTF-8 ${filename}tokens.csv -o ${filename}tokens_utf8.csv
    curl --location --request POST "${base_url}/tokens" --data-binary "@${filename}tokens_utf8.csv" --header 'Content-Type: text/csv' --header "x-access-token: ${token}"

    echo "zones"
    iconv -f WINDOWS-1250 -t UTF-8 ${filename}zones.csv -o ${filename}zones_utf8.csv
    curl --location --request POST "${base_url}/zones" --data-binary "@${filename}/zones_utf8.csv" --header 'Content-Type: text/csv' --header "x-access-token: ${token}"

    echo "Finnished: ${filename}"
done
```
