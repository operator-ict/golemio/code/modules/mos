import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMATicketInspectionsDto } from "./interfaces/IMATicketInspectionsDto";

export class MATicketInspectionsDto extends Model<IMATicketInspectionsDto> implements IMATicketInspectionsDto {
    declare date: string;
    declare lat: number;
    declare lon: number;
    declare reason: string;
    declare result: boolean;
    declare user_id: string;

    public static attributeModel: ModelAttributes<MATicketInspectionsDto> = {
        date: DataTypes.DATE,
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        lat: DataTypes.DECIMAL,
        lon: DataTypes.DECIMAL,
        reason: DataTypes.STRING,
        result: DataTypes.BOOLEAN,
        user_id: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IMATicketInspectionsDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                date: { type: "string", format: "date-time" },
                lat: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                lon: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                reason: { type: "string" },
                result: { type: "boolean" },
                user_id: { type: "string" },
            },
            required: ["date", "result", "user_id"],
        },
    };
}
