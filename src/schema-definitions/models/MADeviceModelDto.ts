import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMADeviceModelDto } from "./interfaces/IMADeviceModelDto";

export class MADeviceModelDto extends Model<IMADeviceModelDto> implements IMADeviceModelDto {
    declare count: number;
    declare model: string;

    public static attributeModel: ModelAttributes<MADeviceModelDto> = {
        count: DataTypes.INTEGER,
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        model: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IMADeviceModelDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                count: { type: "integer" },
                model: { type: "string" },
            },
            required: ["count"],
        },
    };
}
