import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBEPlacesDto } from "./interfaces/IBEPlacesDto";
import { IBEVisitsDto } from "./interfaces/IBEVisitsDto";
import { IBEVouchersDto } from "./interfaces/IBEVouchersDto";

export class BEVouchersDto extends Model<IBEVouchersDto> implements IBEVouchersDto {
    declare bundle_id: number;
    declare identifier_type: string;
    declare tariff_profile: string;
    declare customer_profile: string;
    declare valid_from: string;
    declare valid_to: string;

    public static attributeModel: ModelAttributes<BEVouchersDto> = {
        bundle_id: { primaryKey: true, type: DataTypes.INTEGER },
        identifier_type: DataTypes.STRING,
        tariff_profile: DataTypes.STRING,
        customer_profile: DataTypes.STRING,
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
    };

    public static jsonSchema: JSONSchemaType<IBEVouchersDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                bundle_id: { type: "integer" },
                identifier_type: { type: "string" },
                tariff_profile: { type: "string" },
                customer_profile: { type: "string" },
                valid_from: { type: "string" },
                valid_to: { type: "string" },
            },
            required: ["bundle_id", "identifier_type", "tariff_profile", "customer_profile", "valid_from", "valid_to"],
        },
    };
}
