import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBETokenDto } from "./interfaces/IBETokenDto";

export class BETokenDto extends Model<IBETokenDto> implements IBETokenDto {
    declare created: string;
    declare identifier_type: string;
    declare token_id: string;

    public static attributeModel: ModelAttributes<BETokenDto> = {
        created: DataTypes.DATE,
        identifier_type: DataTypes.STRING(50),
        token_id: {
            primaryKey: true,
            type: DataTypes.STRING(50),
        },
    };

    public static jsonSchema: JSONSchemaType<IBETokenDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                created: { type: "string" },
                identifier_type: { type: "string" },
                token_id: { type: "string" },
            },
            required: ["created", "identifier_type", "token_id"],
        },
    };
}
