import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMATicketActivationDto } from "./interfaces/IMATicketActivationDto";

export class MATicketActivationDto extends Model<IMATicketActivationDto> implements IMATicketActivationDto {
    declare date: string;
    declare lat: number | null;
    declare lon: number | null;
    declare ticket_id: number;
    declare type: string | null;
    declare zones: string | null;

    public static attributeModel: ModelAttributes<MATicketActivationDto> = {
        date: DataTypes.DATE,
        lat: DataTypes.DECIMAL,
        lon: DataTypes.DECIMAL,
        ticket_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        type: DataTypes.STRING,
        zones: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IMATicketActivationDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                date: { type: "string", format: "date-time" },
                lat: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                lon: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                ticket_id: { type: "integer" },
                type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                zones: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["date", "ticket_id"],
        },
    };
}
