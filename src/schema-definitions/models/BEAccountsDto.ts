import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBEAccountsDto } from "./interfaces/IBEAccountsDto";

export class BEAccountsDto extends Model<IBEAccountsDto> implements IBEAccountsDto {
    declare count: number;
    declare day: string;

    public static attributeModel: ModelAttributes<BEAccountsDto> = {
        count: DataTypes.INTEGER,
        day: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
    };

    public static jsonSchema: JSONSchemaType<IBEAccountsDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                count: { type: "integer" },
                day: { type: "string" },
            },
            required: ["count", "day"],
        },
    };
}
