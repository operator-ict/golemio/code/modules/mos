import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBEZoneDto } from "./interfaces/IBEZoneDto";

export class BEZoneDto extends Model<IBEZoneDto> implements IBEZoneDto {
    declare coupon_id: number;
    declare zone_name: string;

    public static attributeModel: ModelAttributes<BEZoneDto> = {
        coupon_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        zone_name: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
    };

    public static jsonSchema: JSONSchemaType<IBEZoneDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                coupon_id: { type: "integer" },
                zone_name: { type: "string" },
            },
            required: ["coupon_id", "zone_name"],
        },
    };
}
