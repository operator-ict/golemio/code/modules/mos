import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBECustomerDto } from "./interfaces/IBECustomerDto";

export class BECustomerDto extends Model<IBECustomerDto> implements IBECustomerDto {
    declare customer_id: number;
    declare date_of_birth: string;

    public static attributeModel: ModelAttributes<BECustomerDto> = {
        customer_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        date_of_birth: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IBECustomerDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                customer_id: { type: "integer" },
                date_of_birth: { type: "string" },
            },
            required: ["customer_id", "date_of_birth"],
        },
    };
}
