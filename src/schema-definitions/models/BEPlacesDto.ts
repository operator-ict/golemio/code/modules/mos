import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBEPlacesDto } from "./interfaces/IBEPlacesDto";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { Point } from "geojson";
export class BEPlacesDto extends Model<IBEPlacesDto> implements IBEPlacesDto {
    declare place_id: number;
    declare place_name: string;
    declare geom: Point;

    public static attributeModel: ModelAttributes<BEPlacesDto> = {
        place_id: { primaryKey: true, type: DataTypes.INTEGER },
        geom: DataTypes.GEOMETRY,
        place_name: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IBEPlacesDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                place_id: { type: "integer" },
                place_name: { type: "string" },
                geom: { $ref: "#/definitions/geometry" },
            },
            required: ["place_id", "place_name"],
        },
        definitions: {
            // @ts-expect-error referenced definition from other file
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
