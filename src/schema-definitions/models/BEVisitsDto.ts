import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBEVisitsDto } from "./interfaces/IBEVisitsDto";

export class BEVisitsDto extends Model<IBEVisitsDto> implements IBEVisitsDto {
    declare place_id: number;
    declare bundle_id: number;
    declare visited: string;

    public static attributeModel: ModelAttributes<BEVisitsDto> = {
        place_id: { primaryKey: true, type: DataTypes.INTEGER },
        bundle_id: { primaryKey: true, type: DataTypes.INTEGER },
        visited: { primaryKey: true, type: DataTypes.DATE },
    };

    public static jsonSchema: JSONSchemaType<IBEVisitsDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                place_id: { type: "integer" },
                bundle_id: { type: "integer" },
                visited: { type: "string" },
            },
            required: ["place_id", "bundle_id", "visited"],
        },
    };
}
