import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBECouponsDto } from "./interfaces/IBECouponsDto";

export class BECouponsDto extends Model<IBECouponsDto> implements IBECouponsDto {
    declare coupon_custom_status_id: number;
    declare coupon_id: number;
    declare created: string;
    declare created_by_id: string;
    declare customer_id: number;
    declare customer_profile_name: string;
    declare description: string;
    declare order_payment_type: number;
    declare order_status: number;
    declare price: number;
    declare seller_id: number;
    declare tariff_id: number;
    declare tariff_int_name: string;
    declare tariff_name: string;
    declare tariff_profile_name: string;
    declare token_id: string;
    declare valid_from: string;
    declare valid_till: string;

    public static attributeModel: ModelAttributes<BECouponsDto> = {
        coupon_custom_status_id: DataTypes.INTEGER,
        coupon_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        created: DataTypes.STRING,
        created_by_id: DataTypes.STRING(50),
        customer_id: DataTypes.INTEGER,
        customer_profile_name: DataTypes.STRING,
        description: DataTypes.STRING,
        order_payment_type: DataTypes.INTEGER,
        order_status: DataTypes.INTEGER,
        price: DataTypes.DECIMAL,
        seller_id: DataTypes.INTEGER,
        tariff_id: DataTypes.INTEGER,
        tariff_int_name: DataTypes.STRING,
        tariff_name: DataTypes.STRING,
        tariff_profile_name: DataTypes.STRING,
        token_id: DataTypes.STRING(50),
        valid_from: DataTypes.STRING,
        valid_till: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IBECouponsDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                coupon_custom_status_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                coupon_id: { type: "integer" },
                created: { type: "string" },
                created_by_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                customer_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                customer_profile_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                order_payment_type: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                order_status: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                price: { type: "number" },
                seller_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                tariff_id: { type: "integer" },
                tariff_int_name: { type: "string" },
                tariff_name: { type: "string" },
                tariff_profile_name: { type: "string" },
                token_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                valid_from: { type: "string" },
                valid_till: { type: "string" },
            },
            required: [
                "coupon_id",
                "created",
                "price",
                "tariff_int_name",
                "tariff_name",
                "tariff_profile_name",
                "valid_from",
                "valid_till",
            ],
        },
    };
}
