import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMATicketPurchasesDto } from "./interfaces/IMATicketPurchasesDto";

export class MATicketPurchasesDto extends Model<IMATicketPurchasesDto> implements IMATicketPurchasesDto {
    declare account_id: string;
    declare cptp: number;
    declare date: string;
    declare duration: number;
    declare lat: number;
    declare lon: number;
    declare tariff_id: number;
    declare tariff_name: string;
    declare ticket_id: number;
    declare zone_count: number;

    public static attributeModel: ModelAttributes<MATicketPurchasesDto> = {
        account_id: DataTypes.STRING,
        cptp: DataTypes.INTEGER,
        date: DataTypes.DATE,
        duration: DataTypes.INTEGER,
        lat: DataTypes.DECIMAL,
        lon: DataTypes.DECIMAL,
        tariff_id: DataTypes.INTEGER,
        tariff_name: DataTypes.STRING,
        ticket_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        zone_count: DataTypes.INTEGER,
    };

    public static jsonSchema: JSONSchemaType<IMATicketPurchasesDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                account_id: { type: "string" },
                cptp: { type: "number" },
                date: { type: "string", format: "date-time" },
                duration: { type: "number" },
                lat: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                lon: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                tariff_id: { type: "number" },
                tariff_name: { type: "string" },
                ticket_id: { type: "number" },
                zone_count: { type: "number" },
            },
            required: ["account_id", "cptp", "date", "ticket_id"],
        },
    };
}
