export interface IMosBEVouchersPvPMessage {
    PVPBundleID: string;
    IdentifierType: string;
    TariffProfileName: string;
    CustomerProfileName: string;
    ValidFrom: string;
    ValidTill: string;
}
