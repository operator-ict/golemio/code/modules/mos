export interface IBECustomerDto {
    customer_id: number;
    date_of_birth: string;
}
