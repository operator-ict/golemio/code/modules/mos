import { Point } from "@golemio/core/dist/shared/geojson";

export interface IBEPlacesDto {
    place_id: number;
    place_name: string;
    geom: Point;
}
