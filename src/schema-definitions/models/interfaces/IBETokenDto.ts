export interface IBETokenDto {
    created: string;
    identifier_type: string;
    token_id: string;
}
