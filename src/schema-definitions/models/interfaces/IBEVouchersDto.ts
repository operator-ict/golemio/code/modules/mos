export interface IBEVouchersDto {
    bundle_id: number;
    identifier_type: string;
    tariff_profile: string;
    customer_profile: string;
    valid_from: string;
    valid_to: string;
}
