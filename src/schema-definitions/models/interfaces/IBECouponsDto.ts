export interface IBECouponsDto {
    coupon_custom_status_id: number | null;
    coupon_id: number;
    created: string;
    created_by_id: string | null;
    customer_id: number | null;
    customer_profile_name: string | null;
    description: string | null;
    order_payment_type: number | null;
    order_status: number | null;
    price: number;
    seller_id: number | null;
    tariff_id: number;
    tariff_int_name: string;
    tariff_name: string;
    tariff_profile_name: string;
    token_id: string | null;
    valid_from: string;
    valid_till: string;
}
