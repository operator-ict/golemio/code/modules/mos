export interface IMATicketActivationDto {
    date: string;
    lat: number | null;
    lon: number | null;
    ticket_id: number;
    type: string | null;
    zones: string | null;
}
