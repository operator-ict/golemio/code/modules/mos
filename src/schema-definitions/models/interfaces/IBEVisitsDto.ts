export interface IBEVisitsDto {
    place_id: number;
    bundle_id: number;
    visited: string;
}
