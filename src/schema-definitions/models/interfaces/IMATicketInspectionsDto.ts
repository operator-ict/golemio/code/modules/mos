export interface IMATicketInspectionsDto {
    date: string;
    lat: number | null;
    lon: number | null;
    reason: string;
    result: boolean;
    user_id: string;
}
