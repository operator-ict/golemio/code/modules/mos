export interface IMosBEVisitsPvPMessage {
    PVPAcceptancePlaceID: string;
    PVPBundleID: string;
    Visited: string;
}
