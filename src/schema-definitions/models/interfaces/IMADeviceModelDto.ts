export interface IMADeviceModelDto {
    count: number;
    model: string;
}
