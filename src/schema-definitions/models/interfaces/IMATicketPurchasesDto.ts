export interface IMATicketPurchasesDto {
    account_id: string;
    cptp: number;
    date: string;
    duration: number;
    lat: number | null;
    lon: number | null;
    tariff_id: number;
    tariff_name: string;
    ticket_id: number;
    zone_count: number;
}
