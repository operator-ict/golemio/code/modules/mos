export interface IMosBEPlacesPvPMessage {
    PVPAcceptancePlaceID: string;
    Name: string;
    POILatitude: string;
    POILongitude: string;
}
