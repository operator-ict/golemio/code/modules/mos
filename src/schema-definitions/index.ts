/** MOS Backend (BE) ************************************************************ */
const BEAccountsInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            count: {
                type: "string",
                minLength: 1,
            },
            day: {
                type: "string",
                minLength: 1,
            },
        },
        required: ["count", "day"],
    },
};

const BECouponsInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            CouponCustomStatusID: { type: "string" },
            Created: {
                type: "string",
                minLength: 1,
            },
            CreatedByID: { type: "string" },
            CustomerProfileName: { type: "string" },
            Description: { type: "string" },
            OrderPaymentType: { type: "string" },
            OrderStatus: { type: "string" },
            Price: {
                type: "string",
                minLength: 1,
            },
            SellerID: { type: "string" },
            TariffIntName: {
                type: "string",
                minLength: 1,
            },
            TariffName: {
                type: "string",
                minLength: 1,
            },
            TariffProfileName: {
                type: "string",
                minLength: 1,
            },
            TokenID: { type: "string" },
            ValidFrom: {
                type: "string",
                minLength: 1,
            },
            ValidTill: {
                type: "string",
                minLength: 1,
            },
            couponID: {
                type: "string",
                minLength: 1,
            },
            customerid: { type: "string" },
            tariffID: {
                type: "string",
                minLength: 1,
            },
        },
        required: [
            "Created",
            "Price",
            "TariffIntName",
            "TariffName",
            "TariffProfileName",
            "ValidFrom",
            "ValidTill",
            "couponID",
            "tariffID",
        ],
    },
};

const BECustomersInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            CustomerID: {
                type: "string",
                minLength: 1,
            },
            DateOfBirth: {
                type: "string",
                minLength: 1,
            },
        },
        required: ["CustomerID", "DateOfBirth"],
    },
};

const BEZonesInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            CouponID: {
                type: "string",
                minLength: 1,
            },
            ZoneName: {
                type: "string",
                minLength: 1,
            },
        },
        required: ["CouponID", "ZoneName"],
    },
};

const BETokensInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            Created: {
                type: "string",
                minLength: 1,
            },
            IdentifierType: {
                type: "string",
                minLength: 1,
            },
            TokenID: {
                type: "string",
                minLength: 1,
            },
        },
        required: ["Created", "IdentifierType", "TokenID"],
    },
};

const BEPlacesInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            PVPAcceptancePlaceID: {
                type: "string",
            },
            Name: {
                type: "string",
                minLength: 1,
            },
            POILatitude: { type: ["number", "string", "null"] },
            POILongitude: { type: ["number", "string", "null"] },
        },
        required: ["Name"],
    },
};

const BEVouchersInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            PVPBundleID: { type: "string" },
            IdentifierType: {
                type: "string",
            },
            TariffProfileName: { type: "string" },
            CustomerProfileName: { type: "string" },
            ValidFrom: {
                type: "string",
                pattern: "^\\d{4}.\\d{2}.\\d{2} \\d{2}:\\d{2}:\\d{2}",
                minLength: 1,
            },
            ValidTill: {
                type: "string",
                pattern: "^\\d{4}.\\d{2}.\\d{2} \\d{2}:\\d{2}:\\d{2}",
                minLength: 1,
            },
        },
        required: ["PVPBundleID", "IdentifierType", "TariffProfileName", "CustomerProfileName", "ValidFrom", "ValidTill"],
    },
};

const BEVisitsInputJSONSchema = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        properties: {
            PVPAcceptancePlaceID: { type: "string" },
            PVPBundleID: {
                type: "string",
            },
            Visited: { type: "string", pattern: "^\\d{4}.\\d{2}.\\d{2} \\d{2}:\\d{2}:\\d{2}" },
        },
        required: ["PVPAcceptancePlaceID", "PVPBundleID", "Visited"],
    },
};

/** MobileApplication (MA) ****************************************************** */

// seznam modelu pouzitych zarizeni a jejich cetnost k aktualnimu datu (1x mesicne)
const MADeviceModelsInputDatasourceJsonSchema = {
    definitions: {
        maObj: {
            type: "object",
            properties: { count: { type: "number" }, model: { type: "string" } },
            required: ["count"],
        },
    },

    // main schema
    oneOf: [
        {
            type: "array",
            items: { $ref: "#/definitions/maObj" },
        },
        {
            $ref: "#/definitions/maObj",
        },
    ],
};

// aktivace jizdenek za posledni casove obdobi (1x denne)
const MATicketActivationsInputDatasourceJsonSchema = {
    definitions: {
        maObj: {
            type: "object",
            properties: {
                date: { type: "string", format: "date-time" },
                lat: { type: ["number", "string", "null"] },
                lon: { type: ["number", "string", "null"] },
                ticketId: { type: ["number", "string"] },
                type: { type: ["string", "null"] },
                zones: { type: ["string", "null"] },
            },
            required: ["date", "ticketId"],
        },
    },

    oneOf: [
        {
            type: "array",
            items: { $ref: "#/definitions/maObj" },
        },
        {
            $ref: "#/definitions/maObj",
        },
    ],
};

// kontroly jizdenek za posledni casove obdobi (1x denne)
const MATicketInspectionsInputDatasourceJsonSchema = {
    definitions: {
        maObj: {
            type: "object",
            properties: {
                date: { type: "string", format: "date-time" },
                lat: { type: ["number", "string", "null"] },
                lon: { type: ["number", "string", "null"] },
                reason: { type: "string" },
                result: { type: "boolean" },
                userId: { type: "string" },
            },
            required: ["date", "result", "userId"],
        },
    },

    // main schema
    oneOf: [
        {
            type: "array",
            items: { $ref: "#/definitions/maObj" },
        },
        {
            $ref: "#/definitions/maObj",
        },
    ],
};

// prodeje jizdenek za posledni casove obdobi (1x denne)
const MATicketPurchasesInputDatasourceJsonSchema = {
    definitions: {
        maObj: {
            type: "object",
            properties: {
                accountId: { type: "string" },
                cptp: { type: "number" },
                date: { type: "string", format: "date-time" },
                duration: { type: ["number", "string"] },
                lat: { type: ["number", "string", "null"] },
                lon: { type: ["number", "string", "null"] },
                tariffId: { type: "number" },
                tariffName: { type: "string" },
                ticketId: { type: "number" },
                zoneCount: { type: "number" },
            },
            required: ["accountId", "cptp", "date", "ticketId"],
        },
    },

    // main schema
    oneOf: [
        {
            type: "array",
            items: { $ref: "#/definitions/maObj" },
        },
        {
            $ref: "#/definitions/maObj",
        },
    ],
};

const forExport = {
    BE: {
        accounts: {
            datasourceJsonSchema: BEAccountsInputJSONSchema,
            name: "MOSBEAccounts",
            pgTableName: "mos_be_accounts",
        },
        coupons: {
            datasourceJsonSchema: BECouponsInputJSONSchema,
            name: "MOSBECoupons",
            pgTableName: "mos_be_coupons",
        },
        customers: {
            datasourceJsonSchema: BECustomersInputJSONSchema,
            name: "MOSBECustomers",
            pgTableName: "mos_be_customers",
        },
        name: "MOSBackend",
        tokens: {
            datasourceJsonSchema: BETokensInputJSONSchema,
            name: "MOSBETokens",
            pgTableName: "mos_be_tokens",
        },
        zones: {
            datasourceJsonSchema: BEZonesInputJSONSchema,
            name: "MOSBEZones",
            pgTableName: "mos_be_zones",
        },
        places: {
            datasourceJsonSchema: BEPlacesInputJSONSchema,
            name: "MOSBEPlaces",
            pgTableName: "mos_be_visited_places_pvp",
        },
        vouchers: {
            datasourceJsonSchema: BEVouchersInputJSONSchema,
            name: "MOSBEVouchers",
            pgTableName: "mos_be_vouchers_pvp",
        },
        visits: {
            datasourceJsonSchema: BEVisitsInputJSONSchema,
            name: "MOSBEVisits",
            pgTableName: "mos_be_visits_pvp",
        },
    },
    MA: {
        deviceModels: {
            datasourceJsonSchema: MADeviceModelsInputDatasourceJsonSchema,
            name: "MOSMADeviceModels",
            pgTableName: "mos_ma_devicemodels",
        },
        name: "MOSMobileApplication",
        ticketActivations: {
            datasourceJsonSchema: MATicketActivationsInputDatasourceJsonSchema,
            name: "MOSMATicketActivations",
            pgTableName: "mos_ma_ticketactivations",
        },
        ticketInspections: {
            datasourceJsonSchema: MATicketInspectionsInputDatasourceJsonSchema,
            name: "MOSMATicketInspections",
            pgTableName: "mos_ma_ticketinspections",
        },
        ticketPurchases: {
            datasourceJsonSchema: MATicketPurchasesInputDatasourceJsonSchema,
            name: "MOSMATicketPurchases",
            pgTableName: "mos_ma_ticketpurchases",
        },
    },
    name: "MOS",
    pgSchema: "mos",
};

export { forExport as MOS };
