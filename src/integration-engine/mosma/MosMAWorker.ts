import { MOS } from "#sch";
import { MADeviceModelDto } from "#sch/models/MADeviceModelDto";
import { MATicketActivationDto } from "#sch/models/MATicketActivationDto";
import { MATicketInspectionsDto } from "#sch/models/MATicketInspectionsDto";
import { MATicketPurchasesDto } from "#sch/models/MATicketPurchasesDto";
import { ITransformation } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import {
    MosMADeviceModelsTransformation,
    MosMATicketActivationsTransformation,
    MosMATicketInspectionsTransformation,
    MosMATicketPurchasesTransformation,
} from "./";

export class MosMAWorker extends BaseWorker {
    private deviceModelModel: PostgresModel;
    private ticketActivationsModel: PostgresModel;
    private ticketInspectionsModel: PostgresModel;
    private ticketPurchasesModel: PostgresModel;
    private deviceModelTransformation: MosMADeviceModelsTransformation;
    private ticketActivationsTransformation: MosMATicketActivationsTransformation;
    private ticketInspectionsTransformation: MosMATicketInspectionsTransformation;
    private ticketPurchasesTransformation: MosMATicketPurchasesTransformation;
    private queuePrefix: string;

    constructor() {
        super();
        this.deviceModelModel = new PostgresModel(
            MOS.MA.deviceModels.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: MADeviceModelDto.attributeModel,
                pgTableName: MOS.MA.deviceModels.pgTableName,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(MOS.MA.deviceModels.name + "ModelValidator", MADeviceModelDto.jsonSchema)
        );
        this.ticketActivationsModel = new PostgresModel(
            MOS.MA.ticketActivations.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: MATicketActivationDto.attributeModel,
                pgTableName: MOS.MA.ticketActivations.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.MA.ticketActivations.name + "ModelValidator", MATicketActivationDto.jsonSchema)
        );
        this.ticketInspectionsModel = new PostgresModel(
            MOS.MA.ticketInspections.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: MATicketInspectionsDto.attributeModel,
                pgTableName: MOS.MA.ticketInspections.pgTableName,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(MOS.MA.ticketInspections.name + "ModelValidator", MATicketInspectionsDto.jsonSchema)
        );
        this.ticketPurchasesModel = new PostgresModel(
            MOS.MA.ticketPurchases.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: MATicketPurchasesDto.attributeModel,
                pgTableName: MOS.MA.ticketPurchases.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.MA.ticketPurchases.name + "ModelValidator", MATicketPurchasesDto.jsonSchema)
        );
        this.deviceModelTransformation = new MosMADeviceModelsTransformation();
        this.ticketActivationsTransformation = new MosMATicketActivationsTransformation();
        this.ticketInspectionsTransformation = new MosMATicketInspectionsTransformation();
        this.ticketPurchasesTransformation = new MosMATicketPurchasesTransformation();
        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + MOS.MA.name.toLowerCase();
    }

    public saveDeviceModelsDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.deviceModelTransformation.transform(inputData);
        await this.deviceModelModel.truncate();
        await this.deviceModelModel.save(transformedData);
    };

    public saveTicketActivationsDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.ticketActivationsTransformation.transform(inputData);
        await this.ticketActivationsModel.saveBySqlFunction(transformedData, ["ticket_id"]);
    };

    public saveTicketInspectionsDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.ticketInspectionsTransformation.transform(inputData);
        await this.ticketInspectionsModel.save(transformedData);
    };

    public saveTicketPurchasesDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.ticketPurchasesTransformation.transform(inputData);
        await this.ticketPurchasesModel.saveBySqlFunction(transformedData, ["ticket_id"]);
    };

    public transformAndSaveChunkedData = async (msg: any): Promise<void> => {
        const input = JSON.parse(msg.content.toString());
        const inputData = input.data;
        const transformMethod: keyof MosMAWorker = input.transformMethod;
        const saveMethod: keyof MosMAWorker = input.saveMethod;
        const transformedData = await (this[transformMethod] as unknown as ITransformation).transform(inputData);
        await (this[saveMethod] as unknown as PostgresModel).save(transformedData);
    };
}
