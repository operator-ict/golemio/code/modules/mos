import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosMADeviceModelsTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.MA.deviceModels.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            count: element.count,
            model: element.model,
        };
        return res;
    };
}
