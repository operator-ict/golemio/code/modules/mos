import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosMATicketInspectionsTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.MA.ticketInspections.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            date: element.date,
            lat: element.lat ? parseFloat(element.lat) : null,
            lon: element.lon ? parseFloat(element.lon) : null,
            reason: element.reason,
            result: element.result,
            user_id: element.userId,
        };
        return res;
    };
}
