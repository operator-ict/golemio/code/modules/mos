import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";

export abstract class AbstractArrayBaseTransformation extends BaseTransformation implements ITransformation {
    public abstract name: string;
    protected abstract transformElement: (element: any) => any;

    constructor() {
        super();
    }

    public transform = async (data: any | any[]): Promise<any[]> => {
        if (data instanceof Array) {
            const promises = data.map((element) => {
                return this.transformElement(element);
            });
            const results = await Promise.all(promises);
            return results.filter((r) => r);
        } else {
            return [await this.transformElement(data)];
        }
    };
}
