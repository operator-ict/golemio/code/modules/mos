import { MOS } from "#sch/index";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { MosBEWorker } from "#ie/mosbe";
import { MosMAWorker } from "#ie/mosma";
import { MosBEpVpWorker } from "./mosbe/MosBEPvPWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: MOS.BE.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + MOS.BE.name.toLowerCase(),
        queues: [
            {
                name: "saveAccountsDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosBEWorker,
                workerMethod: "saveAccountsDataToDB",
            },
            {
                name: "saveCouponsDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosBEWorker,
                workerMethod: "saveCouponsDataToDB",
            },
            {
                name: "saveCustomersDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosBEWorker,
                workerMethod: "saveCustomersDataToDB",
            },
            {
                name: "saveTokensDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosBEWorker,
                workerMethod: "saveTokensDataToDB",
            },
            {
                name: "saveZonesDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosBEWorker,
                workerMethod: "saveZonesDataToDB",
            },
        ],
    },
    {
        name: MOS.MA.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + MOS.MA.name.toLowerCase(),
        queues: [
            {
                name: "saveDeviceModelsDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosMAWorker,
                workerMethod: "saveDeviceModelsDataToDB",
            },
            {
                name: "saveTicketActivationsDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosMAWorker,
                workerMethod: "saveTicketActivationsDataToDB",
            },
            {
                name: "saveTicketInspectionsDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosMAWorker,
                workerMethod: "saveTicketInspectionsDataToDB",
            },
            {
                name: "saveTicketPurchasesDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosMAWorker,
                workerMethod: "saveTicketPurchasesDataToDB",
            },
            {
                name: "transformAndSaveChunkedData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: MosMAWorker,
                workerMethod: "transformAndSaveChunkedData",
            },
        ],
    },
];

export { queueDefinitions };
