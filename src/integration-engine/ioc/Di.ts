import { BEVisitsDto } from "./../../schema-definitions/models/BEVisitsDto";
import { MosBEPlacesPvPTransformation } from "#ie/mosbe/MosBEPlacesPvPTransformation";
import { MosBEVisitsPvPTransformation } from "#ie/mosbe/MosBEVisitsPvPTransformation";
import { MosBEVouchersPvPTransformation } from "#ie/mosbe/MosBEVouchersPvPTransformation";
import { BEPlacesDto } from "#sch/models/BEPlacesDto";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { BEVouchersDto } from "#sch/models/BEVouchersDto";
import { MosBEPlacesPvPTRepository } from "#ie/mosbe/repositories/MosBEPlacesPvPTRepository";
import { MosBEVisitsPvPTaskRepository } from "#ie/mosbe/repositories/MosBEVisitsPvPTaskRepository";
import MosBEPlacesPvPTask from "#ie/mosbe/tasks/MosBEPlacesPvPTask";
import MosBEVisitsPvPTask from "#ie/mosbe/tasks/MosBEVisitsPvPTask";
import MosBEVouchersPvPTask from "#ie/mosbe/tasks/MosBEVouchersPvPTask";
import { MosBEVouchersPvPRepository } from "#ie/mosbe/repositories/MosBEVouchersPvPRepository";

//container
const praguevisitorpassContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();

//#region Transformations
praguevisitorpassContainer.register(ModuleContainerToken.MosBEPlacesPvPTransformation, MosBEPlacesPvPTransformation);
praguevisitorpassContainer.register(ModuleContainerToken.MosBEVisitsPvPTransformation, MosBEVisitsPvPTransformation);
praguevisitorpassContainer.register(ModuleContainerToken.MosBEVouchersPvPTransformation, MosBEVouchersPvPTransformation);
//#endregion

//#region Repositories
praguevisitorpassContainer.register(ModuleContainerToken.MosBEPlacesPvPTRepository, MosBEPlacesPvPTRepository);
praguevisitorpassContainer.register(ModuleContainerToken.MosBEVisitsPvPTaskRepository, MosBEVisitsPvPTaskRepository);
praguevisitorpassContainer.register(ModuleContainerToken.MosBEVouchersPvPRepository, MosBEVouchersPvPRepository);

//#endregion

//#region Tasks
praguevisitorpassContainer.registerSingleton(ModuleContainerToken.MosBEPlacesPvPTask, MosBEPlacesPvPTask);
praguevisitorpassContainer.registerSingleton(ModuleContainerToken.MosBEVisitsPvPTask, MosBEVisitsPvPTask);
praguevisitorpassContainer.registerSingleton(ModuleContainerToken.MosBEVouchersPvPTask, MosBEVouchersPvPTask);

//#endregion

export { praguevisitorpassContainer as PraguevisitorpassContainer };
