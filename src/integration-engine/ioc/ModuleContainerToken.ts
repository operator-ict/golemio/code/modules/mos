const ModuleContainerToken = {
    MosBEPlacesPvPTransformation: Symbol(),
    MosBEVisitsPvPTransformation: Symbol(),
    MosBEVouchersPvPTransformation: Symbol(),
    MosBEVisitsPvPTaskRepository: Symbol(),
    MosBEPlacesPvPTRepository: Symbol(),
    MosBEVouchersPvPRepository: Symbol(),
    MosBEPlacesPvPTask: Symbol(),
    MosBEVisitsPvPTask: Symbol(),
    MosBEVouchersPvPTask: Symbol(),
    BEPlacesDto: Symbol(),
    BEVouchersDto: Symbol(),
    BEVisitsDto: Symbol(),
};

export { ModuleContainerToken };
