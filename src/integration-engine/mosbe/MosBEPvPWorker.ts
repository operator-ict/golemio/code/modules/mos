import { PraguevisitorpassContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MODULE_NAME } from "#sch/models/PvPContants";
import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import MosBEPlacesPvPTask from "./tasks/MosBEPlacesPvPTask";
import MosBEVisitsPvPTask from "./tasks/MosBEVisitsPvPTask";
import MosBEVouchersPvPTask from "./tasks/MosBEVouchersPvPTask";

export class MosBEpVpWorker extends AbstractWorker {
    protected name = MODULE_NAME;

    constructor() {
        super();
        this.registerTask(PraguevisitorpassContainer.resolve<MosBEPlacesPvPTask>(ModuleContainerToken.MosBEPlacesPvPTask));
        this.registerTask(PraguevisitorpassContainer.resolve<MosBEVisitsPvPTask>(ModuleContainerToken.MosBEVisitsPvPTask));
        this.registerTask(PraguevisitorpassContainer.resolve<MosBEVouchersPvPTask>(ModuleContainerToken.MosBEVouchersPvPTask));
    }
}
