import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { IBEPlacesDto } from "#sch/models/interfaces/IBEPlacesDto";
import { IMosBEPlacesPvPMessage } from "#sch/models/interfaces/IMosBEPlacesPvPMessage";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBEPlacesPvPTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.places.name;
    }

    protected transformElement = async (element: IMosBEPlacesPvPMessage): Promise<IBEPlacesDto> => {
        return {
            place_id: parseInt(element.PVPAcceptancePlaceID, 10),
            place_name: element.Name,
            geom: {
                type: "Point",
                coordinates: [Number.parseFloat(element.POILongitude), Number.parseFloat(element.POILatitude)],
            },
        };
    };
}
