import { MOS } from "#sch";
import { BEAccountsDto } from "#sch/models/BEAccountsDto";
import { BECouponsDto } from "#sch/models/BECouponsDto";
import { BECustomerDto } from "#sch/models/BECustomerDto";
import { BETokenDto } from "#sch/models/BETokenDto";
import { BEZoneDto } from "#sch/models/BEZoneDto";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import {
    MosBEAccountsTransformation,
    MosBECouponsTransformation,
    MosBECustomersTransformation,
    MosBETokensTransformation,
    MosBEZonesTransformation,
} from "./";

export class MosBEWorker extends BaseWorker {
    private accountsModel: PostgresModel;
    private couponsModel: PostgresModel;
    private customersModel: PostgresModel;
    private tokensModel: PostgresModel;
    private zonesModel: PostgresModel;
    private accountsTransformation: MosBEAccountsTransformation;
    private couponsTransformation: MosBECouponsTransformation;
    private customersTransformation: MosBECustomersTransformation;
    private tokensTransformation: MosBETokensTransformation;
    private zonesTransformation: MosBEZonesTransformation;

    constructor() {
        super();
        this.accountsModel = new PostgresModel(
            MOS.BE.accounts.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: BEAccountsDto.attributeModel,
                pgTableName: MOS.BE.accounts.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.accounts.name + "ModelValidator", BEAccountsDto.jsonSchema)
        );
        this.couponsModel = new PostgresModel(
            MOS.BE.coupons.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: BECouponsDto.attributeModel,
                pgTableName: MOS.BE.coupons.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.coupons.name + "ModelValidator", BECouponsDto.jsonSchema)
        );
        this.customersModel = new PostgresModel(
            MOS.BE.customers.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: BECustomerDto.attributeModel,
                pgTableName: MOS.BE.customers.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.customers.name + "ModelValidator", BECustomerDto.jsonSchema)
        );
        this.tokensModel = new PostgresModel(
            MOS.BE.tokens.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: BETokenDto.attributeModel,
                pgTableName: MOS.BE.tokens.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.tokens.name + "ModelValidator", BETokenDto.jsonSchema)
        );
        this.zonesModel = new PostgresModel(
            MOS.BE.zones.name + "Model",
            {
                pgSchema: MOS.pgSchema,
                outputSequelizeAttributes: BEZoneDto.attributeModel,
                pgTableName: MOS.BE.zones.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.zones.name + "ModelValidator", BEZoneDto.jsonSchema)
        );
        this.accountsTransformation = new MosBEAccountsTransformation();
        this.couponsTransformation = new MosBECouponsTransformation();
        this.customersTransformation = new MosBECustomersTransformation();
        this.tokensTransformation = new MosBETokensTransformation();
        this.zonesTransformation = new MosBEZonesTransformation();
    }

    public saveAccountsDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.accountsTransformation.transform(inputData);
        await this.accountsModel.saveBySqlFunction(transformedData, ["day"]);
    };

    public saveCouponsDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.couponsTransformation.transform(inputData);
        await this.couponsModel.saveBySqlFunction(transformedData, ["coupon_id"]);
    };

    public saveCustomersDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.customersTransformation.transform(inputData);
        await this.customersModel.saveBySqlFunction(transformedData, ["customer_id"]);
    };

    public saveTokensDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.tokensTransformation.transform(inputData);
        await this.tokensModel.saveBySqlFunction(transformedData, ["token_id"]);
    };

    public saveZonesDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.zonesTransformation.transform(inputData);
        await this.zonesModel.saveBySqlFunction(transformedData, ["coupon_id", "zone_name"]);
    };
}
