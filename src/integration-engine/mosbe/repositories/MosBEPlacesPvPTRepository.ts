import { MOS } from "#sch/index";
import { BEPlacesDto } from "#sch/models/BEPlacesDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MosBEPlacesPvPTRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MOS.BE.places.name + "Repository",
            {
                outputSequelizeAttributes: BEPlacesDto.attributeModel,
                pgTableName: MOS.BE.places.pgTableName,
                pgSchema: MOS.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.places.name + "Validator", BEPlacesDto.jsonSchema)
        );
    }
}
