import { BEVisitsDto } from "./../../../schema-definitions/models/BEVisitsDto";
import { MOS } from "#sch/index";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MosBEVisitsPvPTaskRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MOS.BE.visits.name + "Repository",
            {
                outputSequelizeAttributes: BEVisitsDto.attributeModel,
                pgTableName: MOS.BE.visits.pgTableName,
                pgSchema: MOS.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.visits.name + "Validator", BEVisitsDto.jsonSchema)
        );
    }
}
