import { MOS } from "#sch/index";
import { BEVouchersDto } from "#sch/models/BEVouchersDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MosBEVouchersPvPRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MOS.BE.vouchers.name + "Repository",
            {
                outputSequelizeAttributes: BEVouchersDto.attributeModel,
                pgTableName: MOS.BE.vouchers.pgTableName,
                pgSchema: MOS.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(MOS.BE.vouchers.name + "Validator", BEVouchersDto.jsonSchema)
        );
    }
}
