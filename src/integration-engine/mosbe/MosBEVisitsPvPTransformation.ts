import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { IBEVisitsDto } from "#sch/models/interfaces/IBEVisitsDto";
import { IMosBEVisitsPvPMessage } from "#sch/models/interfaces/IMosBEVisitsPvPMessage";
import { DateTime } from "@golemio/core/dist/helpers";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBEVisitsPvPTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.visits.name;
    }

    protected transformElement = async (element: IMosBEVisitsPvPMessage): Promise<IBEVisitsDto> => {
        const res = {
            place_id: parseInt(element.PVPAcceptancePlaceID, 10),
            bundle_id: parseInt(element.PVPBundleID, 10),
            visited: DateTime.fromFormat(element.Visited, "yyyy-LL-dd HH:mm:ss", { timeZone: "Europe/Prague" }).toISOString(),
        };
        return res;
    };
}
