import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBECustomersTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.customers.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            customer_id: parseInt(element.CustomerID, 10),
            date_of_birth: element.DateOfBirth,
        };
        return res;
    };
}
