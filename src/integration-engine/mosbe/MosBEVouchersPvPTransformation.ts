import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { IBEVouchersDto } from "#sch/models/interfaces/IBEVouchersDto";
import { IMosBEVouchersPvPMessage } from "#sch/models/interfaces/IMosBEVouchersPvPMessage";
import { DateTime } from "@golemio/core/dist/helpers/DateTime";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBEVouchersPvPTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.vouchers.name;
    }

    protected transformElement = async (element: IMosBEVouchersPvPMessage): Promise<IBEVouchersDto> => {
        const res = {
            bundle_id: parseInt(element.PVPBundleID, 10),
            identifier_type: element.IdentifierType,
            tariff_profile: element.TariffProfileName,
            customer_profile: element.CustomerProfileName,
            valid_from: DateTime.fromFormat(element.ValidFrom, "yyyy-LL-dd HH:mm:ss", {
                timeZone: "Europe/Prague",
            }).toISOString(),
            valid_to: DateTime.fromFormat(element.ValidTill, "yyyy-LL-dd HH:mm:ss", { timeZone: "Europe/Prague" }).toISOString(),
        };
        return res;
    };
}
