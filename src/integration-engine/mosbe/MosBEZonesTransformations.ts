import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBEZonesTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.zones.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            coupon_id: parseInt(element.CouponID, 10),
            zone_name: element.ZoneName,
        };
        return res;
    };
}
