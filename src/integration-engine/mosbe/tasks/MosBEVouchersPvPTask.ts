import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { pvpQueueDefinitions } from "#sch/models/PvPQueuesDefinitions";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MODULE_NAME } from "#sch/models/PvPContants";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MOS } from "#sch";
import { MosBEVouchersPvPTransformation } from "../MosBEVouchersPvPTransformation";
import { MosBEVouchersPvPRepository } from "../repositories/MosBEVouchersPvPRepository";
import { IMosBEVouchersPvPMessage } from "#sch/models/interfaces/IMosBEVouchersPvPMessage";

@injectable()
export default class MosBEVouchersPvPTask extends AbstractTaskJsonSchema<IMosBEVouchersPvPMessage[]> {
    public readonly queueName = pvpQueueDefinitions.vouchers;
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = new JSONSchemaValidator(MOS.BE.vouchers.name + "Task", MOS.BE.vouchers.datasourceJsonSchema);

    constructor(
        @inject(ModuleContainerToken.MosBEVouchersPvPTransformation)
        private readonly transformation: MosBEVouchersPvPTransformation,
        @inject(ModuleContainerToken.MosBEVouchersPvPRepository) private readonly repository: MosBEVouchersPvPRepository
    ) {
        super(MODULE_NAME);
    }

    protected async execute(msg: IMosBEVouchersPvPMessage[]): Promise<void> {
        try {
            const transformedData = await this.transformation.transform(msg);
            await this.repository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError(`Error while executing MosBEVouchersPvPTask.`, this.constructor.name, err);
        }
    }
}
