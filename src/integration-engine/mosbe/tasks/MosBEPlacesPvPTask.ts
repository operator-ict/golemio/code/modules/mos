import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { pvpQueueDefinitions } from "#sch/models/PvPQueuesDefinitions";
import { MosBEPlacesPvPTransformation } from "../MosBEPlacesPvPTransformation";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MODULE_NAME } from "#sch/models/PvPContants";
import { MosBEPlacesPvPTRepository } from "../repositories/MosBEPlacesPvPTRepository";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MOS } from "#sch";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { IMosBEPlacesPvPMessage } from "#sch/models/interfaces/IMosBEPlacesPvPMessage";

@injectable()
export default class MosBEPlacesPvPTask extends AbstractTaskJsonSchema<IMosBEPlacesPvPMessage[]> {
    public readonly queueName = pvpQueueDefinitions.places;
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = new JSONSchemaValidator(MOS.BE.places.name + "Task", MOS.BE.places.datasourceJsonSchema);

    constructor(
        @inject(ModuleContainerToken.MosBEPlacesPvPTransformation) private readonly transformation: MosBEPlacesPvPTransformation,
        @inject(ModuleContainerToken.MosBEPlacesPvPTRepository) private readonly repository: MosBEPlacesPvPTRepository
    ) {
        super(MODULE_NAME);
    }

    protected async execute(msg: IMosBEPlacesPvPMessage[]): Promise<void> {
        try {
            const transformedData = await this.transformation.transform(msg);

            await this.repository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError(`Error while executing MosBEPlacesPvPTask.`, this.constructor.name, err);
        }
    }
}
