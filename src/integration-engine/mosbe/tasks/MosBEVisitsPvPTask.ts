import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { pvpQueueDefinitions } from "#sch/models/PvPQueuesDefinitions";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MosBEVisitsPvPTaskRepository } from "../repositories/MosBEVisitsPvPTaskRepository";
import { MODULE_NAME } from "#sch/models/PvPContants";
import { MosBEVisitsPvPTransformation } from "../MosBEVisitsPvPTransformation";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MOS } from "#sch";
import { IMosBEVisitsPvPMessage } from "#sch/models/interfaces/IMosBEVisitsPvPMessage";

@injectable()
export default class MosBEVisitsPvPTask extends AbstractTaskJsonSchema<IMosBEVisitsPvPMessage[]> {
    public readonly queueName = pvpQueueDefinitions.visits;
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = new JSONSchemaValidator(MOS.BE.visits.name + "Task", MOS.BE.visits.datasourceJsonSchema);

    constructor(
        @inject(ModuleContainerToken.MosBEVisitsPvPTransformation) private readonly transformation: MosBEVisitsPvPTransformation,
        @inject(ModuleContainerToken.MosBEVisitsPvPTaskRepository) private readonly repository: MosBEVisitsPvPTaskRepository
    ) {
        super(MODULE_NAME);
    }

    protected async execute(msg: IMosBEVisitsPvPMessage[]): Promise<void> {
        try {
            const transformedData = await this.transformation.transform(msg);

            await this.repository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError(`Error while executing MosBEVisitsPvPTask.`, this.constructor.name, err);
        }
    }
}
