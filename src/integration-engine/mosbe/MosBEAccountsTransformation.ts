import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBEAccountsTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.accounts.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            count: parseInt(element.count, 10),
            day: element.day,
        };
        return res;
    };
}
