import { AbstractArrayBaseTransformation } from "#ie/helpers/AbstractArrayBaseTransformation";
import { MOS } from "#sch/index";
import { ITransformation } from "@golemio/core/dist/integration-engine";

export class MosBETokensTransformation extends AbstractArrayBaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MOS.BE.tokens.name;
    }

    protected transformElement = async (element: any): Promise<any> => {
        const res = {
            created: element.Created,
            identifier_type: element.IdentifierType,
            token_id: element.TokenID,
        };
        return res;
    };
}
