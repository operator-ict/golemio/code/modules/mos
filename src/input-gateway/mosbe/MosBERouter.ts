import { NumberOfRecordsEventData } from "@golemio/core/dist/helpers/logger/interfaces/INumberOfRecordsEventData";
import { LoggerEventType, checkContentTypeMiddleware, loggerEvents } from "@golemio/core/dist/input-gateway";
import { ContainerToken } from "@golemio/core/dist/input-gateway/ioc/ContainerToken";
import { InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc/Di";
import { CsvParserMiddleware } from "@golemio/core/dist/input-gateway/middleware/CsvParserMiddleware";
import { ICsvParserOptions } from "@golemio/core/dist/input-gateway/middleware/interfaces/ICsvParserOptions";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import {
    MosBEAccountsController,
    MosBECouponsController,
    MosBECustomersController,
    MosBETokensController,
    MosBEZonesController,
} from "./";
import { MosBEPlacesPvPController } from "./MosBEPlacesPvPController";
import { MosBEVisitsPvPController } from "./MosBEVisitsPvPController";
import { MosBEVouchersPvPController } from "./MosBEVouchersPvPController";

export class MosBERouter {
    public router: Router;
    private csvParser: CsvParserMiddleware;
    private accountsController: MosBEAccountsController;
    private couponsController: MosBECouponsController;
    private customersController: MosBECustomersController;
    private tokensController: MosBETokensController;
    private zonesController: MosBEZonesController;
    private placesPvPController: MosBEPlacesPvPController;
    private visitsPvPController: MosBEVisitsPvPController;
    private vouchersPvPController: MosBEVouchersPvPController;

    constructor() {
        this.router = Router();
        this.csvParser = InputGatewayContainer.resolve<CsvParserMiddleware>(ContainerToken.CsvParserMiddleware);
        this.accountsController = new MosBEAccountsController();
        this.couponsController = new MosBECouponsController();
        this.customersController = new MosBECustomersController();
        this.tokensController = new MosBETokensController();
        this.zonesController = new MosBEZonesController();
        this.placesPvPController = new MosBEPlacesPvPController();
        this.visitsPvPController = new MosBEVisitsPvPController();
        this.vouchersPvPController = new MosBEVouchersPvPController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        const csvParserOptions: ICsvParserOptions = {
            delimiter: ",",
            shouldIncludeHeaders: true,
            shouldTrimValues: true,
        };

        this.router.post(
            "/accounts",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostAccounts
        );

        this.router.post(
            "/coupons",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostCoupons
        );

        this.router.post(
            "/customers",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostCustomers
        );

        this.router.post(
            "/tokens",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostTokens
        );

        this.router.post(
            "/zones",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostZones
        );

        this.router.post(
            "/acceptance-places",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostAcceptancePlaces
        );

        this.router.post(
            "/visited-places",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostVisitorPlaces
        );

        this.router.post(
            "/vouchers",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware(csvParserOptions),
            this.PostVouchers
        );
    };

    private PostAccounts = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.accountsController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostCoupons = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.couponsController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostCustomers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.customersController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostTokens = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.tokensController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostZones = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.zonesController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostAcceptancePlaces = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.placesPvPController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostVouchers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.vouchersPvPController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostVisitorPlaces = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.visitsPvPController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const mosBERouter = new MosBERouter().router;

export { mosBERouter };
