import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { MOS } from "#sch/index";
import { pvpQueueDefinitions } from "#sch/models/PvPQueuesDefinitions";
import { IMosBEVisitsPvPMessage } from "#sch/models/interfaces/IMosBEVisitsPvPMessage";

export class MosBEVisitsPvPController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super(MOS.BE.name, new JSONSchemaValidator(MOS.BE.visits.name + "Controller", MOS.BE.visits.datasourceJsonSchema));
    }

    /**
     * Data processing
     */
    public processData = async (inputData: IMosBEVisitsPvPMessage[]): Promise<void> => {
        try {
            await this.validator.Validate(inputData);
            await this.sendMessageToExchange(
                "input." + this.queuePrefix + "." + pvpQueueDefinitions.visits,
                JSON.stringify(inputData),
                {
                    persistent: true,
                }
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
