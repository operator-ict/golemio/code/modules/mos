import { NumberOfRecordsEventData } from "@golemio/core/dist/helpers/logger/interfaces/INumberOfRecordsEventData";
import { LoggerEventType, checkContentTypeMiddleware, loggerEvents } from "@golemio/core/dist/input-gateway";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import {
    MosMADeviceModelsController,
    MosMATicketActivationsController,
    MosMATicketInspectionsController,
    MosMATicketPurchasesController,
} from "./";

export class MosMARouter {
    public router: Router;
    private deviceModelsController: MosMADeviceModelsController;
    private ticketActivationsController: MosMATicketActivationsController;
    private ticketInspectionsController: MosMATicketInspectionsController;
    private ticketPurchasesController: MosMATicketPurchasesController;

    constructor() {
        this.router = Router();
        this.deviceModelsController = new MosMADeviceModelsController();
        this.ticketActivationsController = new MosMATicketActivationsController();
        this.ticketInspectionsController = new MosMATicketInspectionsController();
        this.ticketPurchasesController = new MosMATicketPurchasesController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post("/device-models", checkContentTypeMiddleware(["application/json"]), this.PostDeviceModels);
        this.router.post("/ticket-activations", checkContentTypeMiddleware(["application/json"]), this.PostTicketActivations);
        this.router.post("/ticket-inspections", checkContentTypeMiddleware(["application/json"]), this.PostTicketInspections);
        this.router.post("/ticket-purchases", checkContentTypeMiddleware(["application/json"]), this.PostTicketPurchases);
    };

    private PostDeviceModels = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.deviceModelsController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostTicketActivations = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.ticketActivationsController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostTicketInspections = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.ticketInspectionsController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private PostTicketPurchases = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.ticketPurchasesController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body && req.body instanceof Array ? req.body.length : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const mosMARouter = new MosMARouter().router;

export { mosMARouter };
