// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("express");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");

const app = express();
const server = http.createServer(app);

const bodyParser = require("body-parser");

const start = async () => {
    await AMQPConnector.connect();

    const { MosMARouter } = require("#ig/mosma/MosMARouter");
    const { MosBERouter } = require("#ig/mosbe/MosBERouter");

    app.use(express.raw({ type: "text/csv" }));

    app.use("/mos-ma", bodyParser.json(), new MosMARouter().router);
    app.use("/mos-be", new MosBERouter().router);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await AMQPConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
