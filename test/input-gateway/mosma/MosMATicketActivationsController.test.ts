import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { MosMATicketActivationsController } from "#ig/mosma";

chai.use(chaiAsPromised);

describe("MosMATicketActivationsController", () => {
    let sandbox: SinonSandbox;
    let controller: MosMATicketActivationsController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new MosMATicketActivationsController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should has processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        await controller.processData({});
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
