import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { mosMARouter } from "#ig/mosma";

const bodyParser = require("body-parser");
chai.use(chaiAsPromised);

describe("MosMARouter", () => {
    // Create clean express instance
    const app = express();
    app.use(bodyParser.json());
    let testDataDeviceModels: any;
    let testDataTicketActivations: any;
    let testDataTicketInspections: any;
    let testDataTicketPurchases: any;

    before(async () => {
        // Connect to MQ
        await AMQPConnector.connect();
        // Mount the tested router to the express instance
        app.use("/mos-ma", mosMARouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("Device Models", () => {
        beforeEach(() => {
            testDataDeviceModels = [
                { count: 1, model: "Samsung" },
                { count: 2, model: "Honor" },
                { count: 2, model: "" },
            ];
        });

        it("should respond with 204 to POST /mos-ma/device-models", (done) => {
            request(app)
                .post("/mos-ma/device-models")
                .send(testDataDeviceModels)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/device-models with bad content type", (done) => {
            request(app)
                .post("/mos-ma/device-models")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/device-models with no content type", (done) => {
            request(app)
                .post("/mos-ma/device-models")
                .send("testDataDeviceModels")
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-ma/device-models with invalid data", (done) => {
            request(app)
                .post("/mos-ma/device-models")
                .send([{ model: "honor" }])
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Ticket Activations", () => {
        beforeEach(() => {
            testDataTicketActivations = [
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    lat: 50.1901156,
                    lon: 15.0209595,
                    ticketId: 1190175,
                    type: null,
                },
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    lat: "50.1901156",
                    lon: "15.0209595",
                    ticketId: 1190175,
                    type: null,
                },
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    ticketId: 1190176,
                    type: "purchaseNow",
                },
            ];
        });

        it("should respond with 204 to POST /mos-ma/ticket-activations", (done) => {
            request(app)
                .post("/mos-ma/ticket-activations")
                .send(testDataTicketActivations)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-activations with bad content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-activations")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-activations with no content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-activations")
                .send("testDataTicketActivations")
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-ma/ticket-activations with invalid data", (done) => {
            request(app)
                .post("/mos-ma/ticket-activations")
                .send([{ accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP", cptp: 101, date: "2019-06-18T13:15:00+02:00" }])
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Ticket Inspections", () => {
        beforeEach(() => {
            testDataTicketInspections = [
                {
                    date: "2018-12-28T11:05:32+01:00",
                    reason: "Mimo platné pásmo",
                    result: false,
                    userId: "KMT7NTAB41K7QKFVG7O935S4F2UK6SNO",
                },
                {
                    date: "2019-01-07T08:54:08+01:00",
                    reason: "Platný",
                    result: true,
                    userId: "KMT7NTAB41K7QKFVG7O935S4F2UK6SNO",
                },
                {
                    date: "2019-01-07T08:54:08+01:00",
                    reason: "",
                    result: true,
                    userId: "KMT7NTAB41K7QKFVG7O935S4F2UK6SNO",
                },
            ];
        });

        it("should respond with 204 to POST /mos-ma/ticket-inspections", (done) => {
            request(app)
                .post("/mos-ma/ticket-inspections")
                .send(testDataTicketInspections)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-inspections with bad content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-inspections")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-inspections with no content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-inspections")
                .send("testDataTicketInspections")
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-ma/ticket-inspections with invalid data", (done) => {
            request(app)
                .post("/mos-ma/ticket-inspections")
                .send([{ date: "2018-12-28T11:05:32+01:00", reason: "Mimo platné pásmo", result: false }])
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Ticket Purchases", () => {
        beforeEach(() => {
            testDataTicketPurchases = [
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    lat: 50.1901156,
                    lon: 15.0209595,
                    ticketId: 1190175,
                },
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    lat: "50.1901156",
                    lon: "15.0209595",
                    ticketId: 1190175,
                },
                {
                    accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP",
                    cptp: 101,
                    date: "2019-06-18T13:15:00+02:00",
                    ticketId: 1190176,
                },
            ];
        });

        it("should respond with 204 to POST /mos-ma/ticket-purchases", (done) => {
            request(app)
                .post("/mos-ma/ticket-purchases")
                .send(testDataTicketPurchases)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-purchases with bad content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-purchases")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-ma/ticket-purchases with no content type", (done) => {
            request(app)
                .post("/mos-ma/ticket-purchases")
                .send("testDataTicketPurchases")
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-ma/ticket-purchases with invalid data", (done) => {
            request(app)
                .post("/mos-ma/ticket-purchases")
                .send([{ accountId: "B92AHCP7R8AEFI0UER66V6OU70ML82OP", cptp: 101, date: "2019-06-18T13:15:00+02:00" }])
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });
});
