import { MosBEPlacesPvPController } from "#ig/mosbe/MosBEPlacesPvPController";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("MosBEPlacesPvPController", () => {
    let sandbox: SinonSandbox;
    let controller: MosBEPlacesPvPController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new MosBEPlacesPvPController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should have processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should process data properly", async () => {
        await controller.processData([
            {
                PVPAcceptancePlaceID: "35154",
                Name: "MobApp",
                POILatitude: "14",
                POILongitude: "14",
            },
        ]);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
