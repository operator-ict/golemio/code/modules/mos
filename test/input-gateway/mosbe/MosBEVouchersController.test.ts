import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { MosBEVouchersPvPController } from "#ig/mosbe/MosBEVouchersPvPController";

chai.use(chaiAsPromised);

describe("MosBEVouchersController", () => {
    let sandbox: SinonSandbox;
    let controller: MosBEVouchersPvPController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new MosBEVouchersPvPController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should have processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should process data properly", async () => {
        await controller.processData([
            {
                PVPBundleID: "35154",
                IdentifierType: "MobApp",
                TariffProfileName: "48 hodi",
                CustomerProfileName: "ObÄŤanskĂ©",
                ValidFrom: "05.09.2023 11:51",
                ValidTill: "07.09.2023 11:51",
            },
        ]);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
