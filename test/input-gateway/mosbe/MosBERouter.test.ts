import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { mosBERouter } from "#ig/mosbe";

chai.use(chaiAsPromised);

describe("MosBERouter", () => {
    // Create clean express instance
    const app = express();
    app.use(express.raw({ type: "text/csv" }));

    let testDataAccounts: string;
    let testDataCoupons: string;
    let testDataCustomers: string;
    let testDataTokens: string;
    let testDataZones: string;

    before(async () => {
        // Connect to MQ
        await AMQPConnector.connect();
        // Mount the tested router to the express instance
        app.use("/mos-be", mosBERouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("Accounts", () => {
        beforeEach(() => {
            testDataAccounts =
                "day,count\r\n" +
                "2018-08-02,4\r\n" +
                "2018-08-20,1\r\n" +
                "2018-08-21,34\r\n" +
                "2018-08-22,25\r\n" +
                "2018-08-23,13\r\n" +
                "2018-08-24,1\r\n" +
                "2018-08-25,10\r\n";
        });

        it("should respond with 204 to POST /mos-be/accounts", (done) => {
            request(app)
                .post("/mos-be/accounts")
                .send(testDataAccounts)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/accounts with bad content type", (done) => {
            request(app)
                .post("/mos-be/accounts")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/accounts with no content type", (done) => {
            request(app)
                .post("/mos-be/accounts")
                .send(testDataAccounts)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-be/accounts with invalid data", (done) => {
            testDataAccounts = "day,count\r\n" + "2018-08-02,4\r\n" + ",1\r\n";
            request(app)
                .post("/mos-be/accounts")
                .send(testDataAccounts)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Coupons", () => {
        beforeEach(() => {
            testDataCoupons =
                "couponID,customerid,tariffID,Price,Description,ValidFrom,ValidTill,Created,SellerID," +
                "CouponCustomStatusID,TariffName,TariffIntName,TariffProfileName,CustomerProfileName,CreatedByID," +
                "OrderStatus,OrderPaymentType,TokenID\r\n" +
                '7371851,2865644,128,550,"None",2020-03-09 00:01:24.327000000,2020-04-08 23:59:59,' +
                "2020-03-09 00:01:24.330000000,1,1,měsíční občanské.,(0120),Měsíční,Občanské,1,2,1,2025092\r\n" +
                '7371852,3059983,128,550,"None",2020-03-09 00:02:05.650000000,2020-04-08 23:59:59,' +
                "2020-03-09 00:02:05.657000000,1,1,měsíční občanské.,(0120),Měsíční,Občanské,1,2,1,1680141\r\n";
        });

        it("should respond with 204 to POST /mos-be/coupons", (done) => {
            request(app)
                .post("/mos-be/coupons")
                .send(testDataCoupons)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/coupons with bad content type", (done) => {
            request(app)
                .post("/mos-be/coupons")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/coupons with no content type", (done) => {
            request(app)
                .post("/mos-be/coupons")
                .send(testDataCoupons)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-be/coupons with invalid data", (done) => {
            testDataCoupons =
                "couponID,customerid,tariffID,Price,Description,ValidFrom,ValidTill,Created," +
                "SellerID,CouponCustomStatusID,TariffName,TariffIntName,TariffProfileName,CustomerProfileName\r\n" +
                "5745208,2744947,135,2240.0000,None,2019-06-24T00:00:00,2019-09-23T23:59:59,2019-06-20T11:40:34.927," +
                "1,1,čtvrtletní občanské.,(0121),Čtvrtletní,Občanské\r\n" +
                ",1588740,135,1480.0000,,2019-07-01T00:00:00,2019-09-30T23:59:59,2019-06-20T11:30:40.990,29,1," +
                "čtvrtletní občanské.,(0121),Čtvrtletní,Občanské\r\n";
            request(app)
                .post("/mos-be/coupons")
                .send(testDataCoupons)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Customers", () => {
        beforeEach(() => {
            testDataCustomers =
                "CustomerID,DateOfBirth\r\n" + "1497159,1966-07-06\r\n" + "1497160,1967-10-27\r\n" + "1497165,1980-05-28\r\n";
        });

        it("should respond with 204 to POST /mos-be/customers", (done) => {
            request(app)
                .post("/mos-be/customers")
                .send(testDataCustomers)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/customers with bad content type", (done) => {
            request(app)
                .post("/mos-be/customers")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/customers with no content type", (done) => {
            request(app)
                .post("/mos-be/customers")
                .send(testDataCustomers)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-be/customers with invalid data", (done) => {
            testDataCustomers =
                "CustomerID,DateOfBirth\r\n" + "1497159,1966-07-06\r\n" + ",1967-10-27\r\n" + "1497165,1980-05-28\r\n";
            request(app)
                .post("/mos-be/customers")
                .send(testDataCustomers)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Tokens", () => {
        beforeEach(() => {
            testDataTokens =
                "TokenID,IdentifierType,Created\r\n" +
                "2405071,MobApp,2020-03-09 00:03:41.013000000\r\n" +
                "2405072,MobApp,2020-03-09 00:07:05.363000000\r\n" +
                "2405073,MobApp,2020-03-09 00:20:04.867000000\r\n" +
                "2405074,MobApp,2020-03-09 00:24:46.923000000\r\n" +
                "2405075,MobApp,2020-03-09 00:26:40.483000000\r\n" +
                "2405076,BPK,2020-03-09 00:41:34.083000000\r\n";
        });

        it("should respond with 204 to POST /mos-be/tokens", (done) => {
            request(app)
                .post("/mos-be/tokens")
                .send(testDataTokens)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/tokens with bad content type", (done) => {
            request(app)
                .post("/mos-be/tokens")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/tokens with no content type", (done) => {
            request(app)
                .post("/mos-be/tokens")
                .send(testDataTokens)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-be/tokens with invalid data", (done) => {
            testDataTokens = "TokenID,IdentifierType\r\n" + "2405071,MobApp\r\n" + "2405072,MobApp\r\n";
            request(app)
                .post("/mos-be/tokens")
                .send(testDataTokens)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Zones", () => {
        beforeEach(() => {
            testDataZones =
                "CouponID,ZoneName\r\n" + "5951672,0+B\r\n" + "5951930,0+B\r\n" + "5952093,0+B\r\n" + "5952198,0+B\r\n";
        });

        it("should respond with 204 to POST /mos-be/zones", (done) => {
            request(app)
                .post("/mos-be/zones")
                .send(testDataZones)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/zones with bad content type", (done) => {
            request(app)
                .post("/mos-be/zones")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /mos-be/zones with no content type", (done) => {
            request(app)
                .post("/mos-be/zones")
                .send(testDataZones)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /mos-be/zones with invalid data", (done) => {
            testDataZones = "CouponID,ZoneName\r\n" + "5951672,0+B\r\n" + ",0+B\r\n";
            request(app)
                .post("/mos-be/zones")
                .send(testDataZones)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });
});
