import { MosBEVisitsPvPController } from "#ig/mosbe/MosBEVisitsPvPController";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("MosBEVisitsPvPController", () => {
    let sandbox: SinonSandbox;
    let controller: MosBEVisitsPvPController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new MosBEVisitsPvPController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should have processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should process data properly", async () => {
        await controller.processData([
            {
                PVPAcceptancePlaceID: "35154",
                PVPBundleID: "1",
                Visited: "07.09.2023 11:51",
            },
        ]);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
