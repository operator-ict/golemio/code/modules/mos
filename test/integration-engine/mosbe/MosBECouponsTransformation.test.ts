import { MosBECouponsTransformation } from "#ie/mosbe";
import { MOS } from "#sch/index";
import { BECouponsDto } from "#sch/models/BECouponsDto";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("MosBECouponsTransformation", () => {
    let transformation: MosBECouponsTransformation;
    let testSourceData: any[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(MOS.BE.coupons.name + "ModelValidator", BECouponsDto.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new MosBECouponsTransformation();
        const buffer = await readFile(__dirname + "/data/mosbe_coupons-input.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MOSBECoupons");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        await expect(validator.Validate(data)).to.be.fulfilled;

        expect(data[0]).to.have.property("coupon_custom_status_id");
        expect(data[0]).to.have.property("coupon_id");
        expect(data[0]).to.have.property("created");
        expect(data[0]).to.have.property("customer_id");
        expect(data[0]).to.have.property("customer_profile_name");
        expect(data[0]).to.have.property("description");
        expect(data[0]).to.have.property("price");
        expect(data[0]).to.have.property("seller_id");
        expect(data[0]).to.have.property("tariff_id");
        expect(data[0]).to.have.property("tariff_int_name");
        expect(data[0]).to.have.property("tariff_name");
        expect(data[0]).to.have.property("tariff_profile_name");
        expect(data[0]).to.have.property("valid_from");
        expect(data[0]).to.have.property("valid_till");
        expect(data[0]).to.have.property("created_by_id");
        expect(data[0]).to.have.property("order_payment_type");
        expect(data[0]).to.have.property("order_status");
        expect(data[0]).to.have.property("token_id");
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("coupon_custom_status_id");
            expect(data[i]).to.have.property("coupon_id");
            expect(data[i]).to.have.property("created");
            expect(data[i]).to.have.property("customer_id");
            expect(data[i]).to.have.property("customer_profile_name");
            expect(data[i]).to.have.property("description");
            expect(data[i]).to.have.property("price");
            expect(data[i]).to.have.property("seller_id");
            expect(data[i]).to.have.property("tariff_id");
            expect(data[i]).to.have.property("tariff_int_name");
            expect(data[i]).to.have.property("tariff_name");
            expect(data[i]).to.have.property("tariff_profile_name");
            expect(data[i]).to.have.property("valid_from");
            expect(data[i]).to.have.property("valid_till");
            expect(data[i]).to.have.property("created_by_id");
            expect(data[i]).to.have.property("order_payment_type");
            expect(data[i]).to.have.property("order_status");
            expect(data[i]).to.have.property("token_id");
        }
    });
});
