import { MosBEVisitsPvPTransformation } from "./../../../src/integration-engine/mosbe/MosBEVisitsPvPTransformation";
import { MOS } from "#sch/index";
import { BEPlacesDto } from "#sch/models/BEPlacesDto";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { MosBEPlacesPvPTransformation } from "#ie/mosbe/MosBEPlacesPvPTransformation";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("MosBEPlacesPvPTransformation", () => {
    let transformation: MosBEPlacesPvPTransformation;
    let testSourceData: any[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(MOS.BE.places.name + "ModelValidator", BEPlacesDto.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new MosBEPlacesPvPTransformation();
        const buffer = await readFile(__dirname + "/data/mosbe_places-input.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MOSBEPlaces");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        await expect(validator.Validate(data)).to.be.fulfilled;

        expect(data[0]).to.have.property("place_id");
        expect(data[0]).to.have.property("place_name");
        expect(data[0]).to.have.property("geom");
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("place_id");
            expect(data[i]).to.have.property("place_name");
            expect(data[i]).to.have.property("geom");
        }
    });
});
