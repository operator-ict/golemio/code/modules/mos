import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { MosBEWorker } from "#ie/mosbe";

describe("MosBEWorker", () => {
    let worker: MosBEWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        worker = new MosBEWorker();
        sandbox.stub(worker["accountsTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["couponsTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["customersTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["tokensTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["zonesTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["accountsModel"], "saveBySqlFunction");
        sandbox.stub(worker["couponsModel"], "saveBySqlFunction");
        sandbox.stub(worker["customersModel"], "saveBySqlFunction");
        sandbox.stub(worker["tokensModel"], "saveBySqlFunction");
        sandbox.stub(worker["zonesModel"], "saveBySqlFunction");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by saveAccountsDataToDB method", async () => {
        await worker.saveAccountsDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["accountsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["accountsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["accountsTransformation"].transform as SinonSpy,
            worker["accountsModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });

    it("should calls the correct methods by saveCouponsDataToDB method", async () => {
        await worker.saveCouponsDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["couponsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["couponsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["couponsTransformation"].transform as SinonSpy,
            worker["couponsModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });

    it("should calls the correct methods by saveCustomersDataToDB method", async () => {
        await worker.saveCustomersDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["customersTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["customersModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["customersTransformation"].transform as SinonSpy,
            worker["customersModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });

    it("should calls the correct methods by saveTokensDataToDB method", async () => {
        await worker.saveTokensDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["tokensTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["tokensModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["tokensTransformation"].transform as SinonSpy,
            worker["tokensModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });

    it("should calls the correct methods by saveZonesDataToDB method", async () => {
        await worker.saveZonesDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["zonesTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["zonesModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["zonesTransformation"].transform as SinonSpy,
            worker["zonesModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });
});
