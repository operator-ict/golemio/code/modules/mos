import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { MosMAWorker } from "#ie/mosma";

describe("MosMAWorker", () => {
    let worker: MosMAWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox
            .stub(PostgresConnector, "getConnection")
            .callsFake(() => Object.assign({ define: sandbox.stub(), query: sandbox.stub() }));

        worker = new MosMAWorker();
        sandbox.stub(worker["deviceModelTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["ticketActivationsTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["ticketInspectionsTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["ticketPurchasesTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["deviceModelModel"], "save");
        sandbox.stub(worker["deviceModelModel"], "truncate");
        sandbox.stub(worker["ticketActivationsModel"], "saveBySqlFunction");
        sandbox.stub(worker["ticketInspectionsModel"], "save");
        sandbox.stub(worker["ticketPurchasesModel"], "saveBySqlFunction");
        sandbox.stub(worker["ticketPurchasesModel"], "save");

        sandbox.stub(worker, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by saveDeviceModelsDataToDB method", async () => {
        await worker.saveDeviceModelsDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["deviceModelTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["deviceModelModel"].truncate as SinonSpy);
        sandbox.assert.calledOnce(worker["deviceModelModel"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["deviceModelTransformation"].transform as SinonSpy,
            worker["deviceModelModel"].truncate as SinonSpy,
            worker["deviceModelModel"].save as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 4);
    });

    it("should calls the correct methods by saveTicketActivationsDataToDB method", async () => {
        await worker.saveTicketActivationsDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["ticketActivationsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ticketActivationsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["ticketActivationsTransformation"].transform as SinonSpy,
            worker["ticketActivationsModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 4);
    });

    it("should calls the correct methods by saveTicketInspectionsDataToDB method", async () => {
        await worker.saveTicketInspectionsDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["ticketInspectionsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ticketInspectionsModel"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["ticketInspectionsTransformation"].transform as SinonSpy,
            worker["ticketInspectionsModel"].save as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 4);
    });

    it("should calls the correct methods by saveTicketPurchasesDataToDB method", async () => {
        await worker.saveTicketPurchasesDataToDB({ content: Buffer.from(JSON.stringify([])) });
        sandbox.assert.calledOnce(worker["ticketPurchasesTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ticketPurchasesModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["ticketPurchasesTransformation"].transform as SinonSpy,
            worker["ticketPurchasesModel"].saveBySqlFunction as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 4);
    });

    it("should calls the correct methods by transformAndSaveChunkedData method", async () => {
        await worker.transformAndSaveChunkedData({
            content: Buffer.from(
                JSON.stringify({
                    data: [],
                    saveMethod: "ticketPurchasesModel",
                    transformMethod: "ticketPurchasesTransformation",
                })
            ),
        });
        sandbox.assert.calledOnce(worker["ticketPurchasesTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ticketPurchasesModel"].save as SinonSpy);
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 4);
    });
});
