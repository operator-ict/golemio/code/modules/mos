import { MosMATicketPurchasesTransformation } from "#ie/mosma";
import { MATicketPurchasesDto } from "#sch/models/MATicketPurchasesDto";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("MosMATicketPurchasesTransformation", () => {
    let transformation: MosMATicketPurchasesTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new MosMATicketPurchasesTransformation();
        const buffer = await readFile(__dirname + "/data/mosma_ticketpurchases-input.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MOSMATicketPurchases");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        expect(data[0]).to.have.property("account_id");
        expect(data[0]).to.have.property("cptp");
        expect(data[0]).to.have.property("date");
        expect(data[0]).to.have.property("duration");
        expect(data[0]).to.have.property("lat");
        expect(data[0]).to.have.property("lon");
        expect(data[0]).to.have.property("tariff_id");
        expect(data[0]).to.have.property("tariff_name");
        expect(data[0]).to.have.property("ticket_id");
        expect(data[0]).to.have.property("zone_count");
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("account_id");
            expect(data[i]).to.have.property("cptp");
            expect(data[i]).to.have.property("date");
            expect(data[i]).to.have.property("duration");
            expect(data[i]).to.have.property("lat");
            expect(data[i]).to.have.property("lon");
            expect(data[i]).to.have.property("tariff_id");
            expect(data[i]).to.have.property("tariff_name");
            expect(data[i]).to.have.property("ticket_id");
            expect(data[i]).to.have.property("zone_count");
        }
    });

    it("should validate input data", async () => {
        const validator = new JSONSchemaValidator("testValidator", MATicketPurchasesDto.jsonSchema);
        const data = await transformation.transform(testSourceData);
        const result = await validator.Validate(data);
        expect(result).to.be.true;
    });
});
