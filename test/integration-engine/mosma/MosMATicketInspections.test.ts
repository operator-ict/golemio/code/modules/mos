import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { MosMATicketInspectionsTransformation } from "#ie/mosma";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MATicketInspectionsDto } from "#sch/models/MATicketInspectionsDto";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("MosMATicketInspectionsTransformation", () => {
    let transformation: MosMATicketInspectionsTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new MosMATicketInspectionsTransformation();
        const buffer = await readFile(__dirname + "/data/mosma_ticketinspections-input.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MOSMATicketInspections");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[1]);
        expect(data[0]).to.have.property("date");
        expect(data[0]).to.have.property("lat");
        expect(data[0]).to.have.property("lon");
        expect(data[0]).to.have.property("reason");
        expect(data[0]).to.have.property("result");
        expect(data[0]).to.have.property("user_id");
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("date");
            expect(data[i]).to.have.property("reason");
            expect(data[i]).to.have.property("result");
            expect(data[i]).to.have.property("user_id");
        }
    });

    it("should validate input data", async () => {
        const validator = new JSONSchemaValidator("testValidator", MATicketInspectionsDto.jsonSchema);
        const data = await transformation.transform(testSourceData);
        const result = await validator.Validate(data);
        expect(result).to.be.true;
    });
});
